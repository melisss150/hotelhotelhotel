import Exceptions.NotAnActualResidentException;
import data.Data;
import data.FileManager;
import userInterface.Interface;

import java.io.IOException;

/**
 * Main class of the project
 */
public class Hotel {

    public static void main(String args[]) throws IOException, NotAnActualResidentException {

        FileManager fileManager = new FileManager();

        Data data = new Data(fileManager.loadBookings(), fileManager.loadReceptionist(), fileManager.loadPassengers(), fileManager.loadRooms());

        Interface graphicInterface = new Interface();

        graphicInterface.startMenu(data);

        fileManager.saveData(data.getBookings(), data.getRecepcionists(), data.getPassengers(), data.getRooms());

    }

}
