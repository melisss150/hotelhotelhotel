package Exceptions;

public class NotAvailableRoomForDateOrType extends NullPointerException {

    public NotAvailableRoomForDateOrType() {
        System.out.println("No hay habitaciones disponibles con esas condiciones.");
    }

    public NotAvailableRoomForDateOrType(String msg) {
        super(msg);
        System.out.println("No hay habitaciones disponibles con esas condiciones.");
    }

}
