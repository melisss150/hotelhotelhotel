package Exceptions;

public class NotAValidReceptionistException extends NullPointerException {

    public NotAValidReceptionistException() {
        System.out.println("Usted no tiene permisos para acceder.");
    }

    public NotAValidReceptionistException(String msg) {
        super(msg);
        System.out.println("Usted no tiene permisos para acceder.");
    }

}
