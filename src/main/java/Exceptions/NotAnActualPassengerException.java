package Exceptions;

public class NotAnActualPassengerException extends NullPointerException {

    public NotAnActualPassengerException() {
        System.out.println("Su dni es incorrecto o no se encuentra registrado en el sistema");
    }

    public NotAnActualPassengerException(String msg) {
        super(msg);
        System.out.println("Su dni es incorrecto o no se encuentra registrado en el sistema");
    }

}
