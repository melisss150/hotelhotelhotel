package Exceptions;

public class NotAValidRoomException extends NullPointerException {

    public NotAValidRoomException() {
        System.out.println("La habitación no existe.");
    }

    public NotAValidRoomException(String msg) {
        super(msg);
        System.out.println("La habitación no existe.");
    }

}
