package Exceptions;

public class NotAnActualResidentException extends Exception {

    public NotAnActualResidentException() {
        System.out.println("Actualmente el pasajero no es residente del hotel.");
    }

    public NotAnActualResidentException(String msg) {
        System.out.println("Actualmente el pasajero no es residente del hotel.");
        System.out.println(msg);
    }

}