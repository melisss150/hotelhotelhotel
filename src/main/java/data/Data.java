package data;

import org.codehaus.jackson.annotate.JsonIgnore;
import reservations.Booking;
import room.Room;
import room.Type;
import users.Passenger;
import users.Receptionist;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class keeps all the information of the hotel for its administration.
 */
public class Data {

    private static List<Booking> bookings;
    private static Map<Integer, Receptionist> receptionists;
    private static Map<Integer, Room> rooms;
    private static Map<Integer, Passenger> passengers;

    /**
     * Constructor for Data object.
     *
     * @param bookings      List
     * @param receptionists Map
     * @param passengers    Map
     * @param rooms         Map
     */
    public Data(List<Booking> bookings, Map<Integer, Receptionist> receptionists, Map<Integer, Passenger> passengers, Map<Integer, Room> rooms) {
        this.bookings = bookings;
        this.receptionists = receptionists;
        this.passengers = passengers;
        this.rooms = rooms;
    }

    /**
     * This method creates a new room and add it to rooms list
     *
     * @param id   Integer
     * @param type Type
     */
    public void createNewRoomAndAdd(Integer id, Type type) {
        Room room = new Room(id, type);
        rooms.put(id, room);
    }

    /**
     * This method creates a new booking and add it to bookings list
     *
     * @param idRoom      Integer
     * @param idPassenger Integer
     * @param startDate   Date
     * @param endDate     Date
     */
    public Booking createNewBooking(Integer idRoom, Integer idPassenger, Date startDate, Date endDate) {
        Booking book = new Booking(idRoom, idPassenger, startDate, endDate);
        bookings.add(book);
        return book;
    }

    /**
     * Get the room list
     *
     * @return Map
     */
    public static Map<Integer, Room> getRooms() {
        return rooms;
    }

    /**
     * This method return all the rooms of a particular type
     *
     * @param type Type
     * @return List of Rooms
     */
    public static List<Room> getRoomsByType(Type type) {
        List<Room> roomsByType = new ArrayList<>();
        Collection<Room> roomCollection = rooms.values();
        for (Room r : roomCollection) {
            if (r.getType().equals(type)) {
                roomsByType.add(r);
            }
        }
        return roomsByType;
    }

    /**
     * Gets the map of passengers
     *
     * @return Map of Passengers
     */
    public Map<Integer, Passenger> getPassengers() {
        return passengers;
    }

    /**
     * Gets the map of receptionists
     *
     * @return Map of Receptionists
     */
    public Map<Integer, Receptionist> getRecepcionists() {
        return receptionists;
    }

    /**
     * Return the list of bookings
     *
     * @return List of Bookings
     */
    public static List<Booking> getBookings() {
        return bookings;
    }

    /**
     * Return a available room from a certain type
     *
     * @param type Type
     * @return Room
     */
    @JsonIgnore
    public Room getAnAvailableRoomByType(Type type) {
        Collection<Room> roomCollection = rooms.values();
        for (Room room : roomCollection) {
            if (room.isAvailable() && room.getType() == type) {
                return room;
            }
        }
        return null;
    }

    /**
     * Gets bookings by passenger, occupied if bol is true  or not occupied if it's false.
     *
     * @param passengerId Integer
     * @return List of Bookings
     */
    public List<Booking> getBookingsByPassengerByStatus(Integer passengerId, Boolean isOccupied) {
        return bookings
                .stream()
                .filter(booking -> (booking.getIdPassenger() == passengerId) &&
                        (booking.isOccupied() == isOccupied) &&
                        (booking.isCharged() == false))
                .collect(Collectors.toList());
    }

    /**
     * Return a list with all bookings that are not finished
     *
     * @return List of Bookings
     */
    public List<Booking> getBookingsNotFinished() {
        return bookings.stream().filter(booking -> booking.isCharged() == false).collect(Collectors.toList());
    }

    /**
     * Return a boolean , true if the room has a reservation made, and false otherwise
     *
     * @return boolean
     */
    public boolean roomHasBookings(Room room) {
        List<Booking> bookings = getBookingsNotFinished();
        for (Booking booking : bookings) {
            if (booking.getIdRoom() == room.getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * It filters the bookings by the id number of the room, and returns it.
     *
     * @return List of Booking
     */
    public List<Booking> getBookingsByRoom(Integer id) {
        return bookings.stream().filter(booking -> booking.getIdRoom() == id).collect(Collectors.toList());
    }

    /**
     * Adds a new booking to the bookings list
     *
     * @param newBooking Booking
     */
    public void addBooking(Booking newBooking) {
        bookings.add(newBooking);
    }

}
