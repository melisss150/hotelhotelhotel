package data;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import reservations.Booking;
import room.Room;
import users.Passenger;
import users.Receptionist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is in charge of managing the files, writes them and reads them.
 */
public class FileManager {

    private File receptionistsFile = new File("src/main/resources/Receptionists");
    private static File passengersFile = new File("src/main/resources/Passengers");
    private File roomsFile = new File("src/main/resources/Rooms");
    private File bookingsFile = new File("src/main/resources/Bookings");
    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Default constructor for FileManager object
     *
     * @throws IOException Exception
     */
    public FileManager() throws IOException {
    }

    /**
     * Principal method to save all data in json files
     *
     * @param bookings      List
     * @param receptionists Map
     * @param passengers    Map
     * @param rooms         Map
     * @throws IOException Exception
     */
    public void saveData(List<Booking> bookings, Map<Integer, Receptionist> receptionists, Map<Integer, Passenger> passengers, Map<Integer, Room> rooms) throws IOException {
        saveBookings(bookings);
        saveRecepcionists(receptionists);
        savePassengers(passengers);
        saveRooms(rooms);
    }

    /**
     * Method to save a list of bookings in json file
     *
     * @param bookings List
     * @throws IOException Exception
     */
    private void saveBookings(List<Booking> bookings) throws IOException {
        objectMapper.writeValue(bookingsFile, bookings);
    }

    /**
     * Method to save a rooms map in json file
     *
     * @param rooms Map
     * @throws IOException Exception
     */
    private void saveRooms(Map<Integer, Room> rooms) throws IOException {
        objectMapper.writeValue(roomsFile, rooms);
    }

    /**
     * Method to save passengers map to json file
     *
     * @param passengers Map
     * @throws IOException Exception
     */
    private void savePassengers(Map<Integer, Passenger> passengers) throws IOException {
        objectMapper.writeValue(passengersFile, passengers);
    }

    /**
     * Method to save receptionist map to json file
     *
     * @param recepcionists Map
     * @throws IOException Exception
     */
    private void saveRecepcionists(Map<Integer, Receptionist> recepcionists) throws IOException {
        objectMapper.writeValue(receptionistsFile, recepcionists);
    }

    /**
     * Method to save rooms map to json file
     *
     * @param rooms Map
     * @param path  String
     * @throws IOException Exception
     */
    public static void saveRooms(Map<Integer, Room> rooms, String path) throws IOException {
        File backUpFile = new File(path);
        objectMapper.writeValue(backUpFile, rooms);
    }

    /**
     * Method to save passengers map to an specific json new file
     *
     * @param passengers Map
     * @param path       String
     * @throws IOException Exception
     */
    public static void savePassengers(Map<Integer, Passenger> passengers, String path) throws IOException {
        File backUpFile = new File(path);
        objectMapper.writeValue(backUpFile, passengers);
    }

    /**
     * Method to save receptionists map to an specific json new file
     *
     * @param recepcionists Map
     * @param path          String
     * @throws IOException Exception
     */
    public static void saveReceptionists(Map<Integer, Receptionist> recepcionists, String path) throws IOException {
        File backUpFile = new File(path);
        objectMapper.writeValue(backUpFile, recepcionists);
    }

    /**
     * Method to save bookings list to an specific json new file
     *
     * @param bookings List
     * @param path     String
     * @throws IOException Exception
     */
    public static void saveBookings(List<Booking> bookings, String path) throws IOException {
        File backUpFile = new File(path);
        objectMapper.writeValue(backUpFile, bookings);
    }

    /**
     * Method to load receptionist map from json file
     *
     * @return Map
     */
    public Map<Integer, Receptionist> loadReceptionist() {
        try {
            return objectMapper.readValue(receptionistsFile, new TypeReference<Map<Integer, Receptionist>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    /**
     * Method to load passengers map from json file
     *
     * @return Map
     */
    public Map<Integer, Passenger> loadPassengers() {
        try {
            return objectMapper.readValue(passengersFile, new TypeReference<Map<Integer, Passenger>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    /**
     * Method to load a map of rooms from json file
     *
     * @return Map
     */
    public Map<Integer, Room> loadRooms() {
        try {
            return objectMapper.readValue(roomsFile, new TypeReference<Map<Integer, Room>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    /**
     * Method to load a list of bookings from json file
     *
     * @return List
     */
    public List<Booking> loadBookings() {
        try {
            return objectMapper.readValue(bookingsFile, new TypeReference<List<Booking>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

}
