package data;

import java.sql.Date;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class handles the user's data entry.
 */
public class DataManager {

    /**
     * Scans an Int value by keyboard and returns it
     *
     * @return int
     */
    public static final int answerInt() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Respuesta: ");
        return sc.nextInt();
    }

    /**
     * It scans an integer value by keyboard and returns it,
     * taking into account a parameter that limits the value entered, it must be less than the value of the parameter.
     *
     * @param lenght int
     * @return int
     */
    public static final int answerInt(int lenght) {
        Scanner sc = new Scanner(System.in);
        int rta = 0;
        do {
            System.out.print("Respuesta: ");
            rta = sc.nextInt();
        } while (rta < 0 || rta > lenght);
        return rta;
    }

    /**
     * Scans an String value by keyboard and returns it
     *
     * @return String
     * @throws InputMismatchException Exception
     */
    public static final String answerString() throws InputMismatchException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Respuesta: ");
        return sc.nextLine();
    }

    /**
     * Function used for the menu,
     * before each menu is displayed again, there will be a pause so it will not be confusing for the user
     */
    public static final void pause() {
        System.out.println("Oprima una tecla para continuar");
        answerString();
    }

    /**
     * Line of space used to make the interface more comfortable
     */
    public static final void separator() {
        System.out.println("----------------------------------------");
    }

    /**
     * Gets today date
     *
     * @return Date
     */
    public static final Date getTodayDate() {
        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        return new Date(currentDate.getTime());
    }

}
