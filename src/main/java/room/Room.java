package room;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * This class have the information of each room.
 */
public class Room {

    private Integer id;
    private Type type;
    private Status status;
    private String statusDetail;


    /**
     * This is the default constructor of Room
     */
    public Room() {
    }

    /**
     * This is the constructor of Room
     *
     * @param id           Integer
     * @param type         Type
     * @param status       Status
     * @param statusDetail String
     */
    public Room(Integer id, Type type, Status status, String statusDetail) {
        this.id = id;
        this.type = type;
        this.status = status;
        this.statusDetail = statusDetail;
    }

    /**
     * This is the constructor for a new Room
     *
     * @param id   Integer
     * @param type Type
     */
    public Room(Integer id, Type type) {
        this.id = id;
        this.type = type;
        this.status = status.AVAILABLE;
        this.statusDetail = "Sin detalles.";
    }

    /**
     * This method sets the room id
     *
     * @param id Integer
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method sets the room status
     *
     * @param status Status
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * This method return if the room is occupied or not
     *
     * @return boolean
     */
    @JsonIgnore
    public boolean isOccupied() {
        return status == Status.OCCUPIED;
    }

    /**
     * This method return if the room is available
     *
     * @return boolean
     */
    @JsonIgnore
    public boolean isAvailable() {
        return status == Status.AVAILABLE;
    }

    /**
     * This method return if the room is not available
     *
     * @return boolean
     */
    @JsonIgnore
    public boolean isNotAvailable() { return status == Status.NOT_AVAILABLE; }

    /**
     * This method return a string with the information of the room
     *
     * @return String
     */
    @Override
    public String toString() {
        String roomDetail = "Id: " + id + " Type: " + type.toString() + ", Status: " + status.toString();
        if (isNotAvailable()) {
            roomDetail += " Status Detail: " + statusDetail;
        }
        return roomDetail;
    }

    /**
     * This method returns the room id
     *
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method returns the room type
     *
     * @return Type
     */
    public Type getType() {
        return type;
    }

    /**
     * This method returns the room Status
     *
     * @return Status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * This method returns the room status detail
     *
     * @return String
     */
    public String getStatusDetail() {
        return statusDetail;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((statusDetail == null) ? 0 : statusDetail.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Room other = (Room) obj;

        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id) && !status.equals(other.status) && !type.equals(type))
            return false;
        return true;
    }

}
