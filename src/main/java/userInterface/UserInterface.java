package userInterface;

import Exceptions.NotAValidRoomException;
import Exceptions.NotAvailableRoomForDateOrType;
import data.Data;
import reservations.Booking;
import room.Room;
import room.Type;
import users.Passenger;

import java.sql.Date;
import java.util.InputMismatchException;
import java.util.List;

import static data.DataManager.answerInt;
import static data.DataManager.separator;

/**
 * This class shows interface required to the user
 */
public class UserInterface {

    /**
     * This method ask to the user what type of room he want
     *
     * @return Type
     */
    public static Type typeMenu() {
        System.out.println("Que tipo de habitación desea?:");
        System.out.println("1- SIMPLE");
        System.out.println("2- DOBLE");
        System.out.println("3- SUITE");
        int rta = answerInt(3);
        separator();
        if (rta == 1) {
            return Type.SIMPLE;
        } else {
            if (rta == 2) {
                return Type.DOUBLE;
            } else {
                return Type.SUITE;
            }
        }
    }

    /**
     * Date menu interface
     *
     * @return Date
     */
    public static final Date dateMenu() {
        int day = 0;
        int month;
        int year;


        do {
            System.out.println("Día?");
            try {
                day = answerInt();
                System.out.println(day);
            } catch (InputMismatchException e) {
                System.out.println("Error: valor no válido");
                day = answerInt();
            }
        } while (day > 31);


        do {
            System.out.println("Mes?");
            try {
                month = answerInt();
            } catch (InputMismatchException e) {
                System.out.println("Error: valor no válido");
                month = answerInt();
            }

        } while (month <= 0 && month > 12);

        do {
            System.out.println("Año?");
            try {
                year = answerInt();
            } catch (InputMismatchException e) {
                System.out.println("Error: valor no válido");
                year = answerInt();
            }

        } while (year < 2018 && year > 2020);

        Date date = new Date(year-1900, month-1, day);
        return date;
    }

    /**
     * This method gets all bookings of a type for an especific passenger and provides the interface to select one
     *
     * @param data        Data
     * @param idPassenger Integer
     * @return Booking
     */
    public static Booking selectBookingFromPassenger(Data data, Integer idPassenger, Boolean bol) {
        List<Booking> allBookingsOccupiedByPassenger = data.getBookingsByPassengerByStatus(idPassenger, bol);

        if (allBookingsOccupiedByPassenger.size() != 0) {
            if (allBookingsOccupiedByPassenger.size() == 1) {
                return allBookingsOccupiedByPassenger.get(0);
            } else {
                System.out.println("Seleccione la opción de la habitacion buscada: ");
                int i = 1;
                Integer rta;
                for (Booking booking : allBookingsOccupiedByPassenger) {
                    System.out.println(i + "- " + "Habitación nº " + booking.getIdRoom());
                    i++;
                }
                rta = answerInt(i);

                if (allBookingsOccupiedByPassenger.get(rta) != null) {
                    return allBookingsOccupiedByPassenger.get(rta);
                } else {
                    throw new NotAValidRoomException();
                }
            }
        }
        return null;
    }

    /**
     * This method creates a reservation for a passanger
     *
     * @param data      Data
     * @param passenger Passenger
     * @return Booking
     */
    public static Booking makeReservationMenu(Data data, Passenger passenger) {
        System.out.println("Ingrese la fecha de llegada: ");
        Date startDate = dateMenu();
        System.out.println("Ingrese la fecha de partida: ");

        Date endDate = dateMenu();
        Type type = typeMenu();
        Booking newBooking = null;
        for (Room room : data.getRoomsByType(type)) {
            if (data.roomHasBookings(room)) {
                for (Booking booking : data.getBookingsByRoom(room.getId())) {
                    if (startDate.compareTo(booking.getEndDate()) == 1 || endDate.compareTo(booking.getStartDate()) == -1) {
                        newBooking = new Booking(passenger.getId(), room.getId(), startDate, endDate);
                        break;
                    }
                }
            } else {
                newBooking = new Booking(passenger.getId(), room.getId(), startDate, endDate);
                break;
            }
        }
        if (newBooking != null) {
            data.addBooking(newBooking);
            return newBooking;
        } else {
            throw new NotAvailableRoomForDateOrType();
        }
    }

}



