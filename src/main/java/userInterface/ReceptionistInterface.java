package userInterface;

import Exceptions.NotAValidReceptionistException;
import Exceptions.NotAValidRoomException;
import Exceptions.NotAnActualResidentException;
import Exceptions.NotAvailableRoomForDateOrType;
import data.Data;
import reservations.Booking;
import room.Room;
import room.Type;
import users.Passenger;
import users.Receptionist;

import java.sql.Date;
import java.util.Calendar;
import java.util.Map;

import static data.DataManager.*;
import static userInterface.PassengerInterface.passengerOnSystemMenu;

/**
 * This class shows receptionist functionality to the user.
 */
public class ReceptionistInterface extends UserInterface {

    /**
     * @param data         Data
     * @param receptionist Receptionist
     * @throws NotAnActualResidentException Exception
     */
    public static void receptionistMenu(Data data, Receptionist receptionist) throws NotAnActualResidentException {
        System.out.println("Que deseas hacer?");
        System.out.println("    1- Check-In");
        System.out.println("    2- Check-Out");
        System.out.println("    3- Hacer reservas");
        System.out.println("    4- Ver estado de habitaciones");
        System.out.println("    5- Ver pasajeros");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt(5);
        separator();
        switch (rta) {
            case 1:
                checkInMenu(data, receptionist);
                break;
            case 2:
                checkOutMenu(data, receptionist);
                break;
            case 3:
                Passenger passenger = passengerOnSystemMenu(data);
                Booking book = makeReservationMenu(data, passenger);
                System.out.println("La reserva hecha es: " + book.toString());
                break;
            case 4:
                roomStatusMenu(data.getRooms());
                break;
            case 5:
                showPassengers(data.getPassengers());
                break;
            case 0:
                Interface.startMenu(data);
                break;
        }
        if (rta != 0) {
            pause();
            receptionistMenu(data, receptionist);
        }
    }

    /**
     * Shows all passengers in the system
     *
     * @param passengers Map of Passengers
     */
    private static void showPassengers(Map<Integer, Passenger> passengers) {
        System.out.println(passengers.toString());
    }

    /**
     * This method restricts the use of the receptionist menu to non-receptionists.
     *
     * @param data Data
     * @throws NotAnActualResidentException Exception
     */
    public static void receptionistValidation(Data data) throws NotAnActualResidentException {
        System.out.print("Bienvenido RECEPCIONISTA, ingrese su DNI: ");
        Receptionist recep = data.getRecepcionists().get(answerInt());
        if (recep != null) {
            System.out.println("Hola " + recep.getName() + "!");
            try {
                receptionistMenu(data, recep);
            } catch (NotAnActualResidentException e) {
                e.printStackTrace();
            }
        } else {
            new NotAValidReceptionistException().getMessage();
            System.out.println("Volverá al menú principal.");
            pause();
            Interface.startMenu(data);
        }

    }

    /**
     * This method shows the administration of the room's status.
     *
     * @param rooms Map of Rooms
     */
    public static void roomStatusMenu(Map<Integer, Room> rooms) {
        System.out.println("    1- Ver estado de todas las habitaciones.");
        System.out.println("    2- Ver habitación específica.");
        System.out.println("(Oprima 0 para volver atras)");
        int rta = answerInt(2);
        separator();
        switch (rta) {
            case 1:
                System.out.println(rooms.toString());
                break;
            case 2:
                System.out.println("Ingrese el numero de habitación: ");
                int idRoom = answerInt();
                Room room = rooms.get(idRoom);
                if (room != null) {
                    System.out.println("La habitacion nro: " + idRoom + " se encuentra: " + room.getStatus());
                    if (!room.getStatusDetail().equals("")) {
                        System.out.println("Debido a: " + room.getStatusDetail());
                    }
                } else {
                    throw new NotAValidRoomException();
                }
                break;
            case 0:
                break;
        }

    }

    /**
     * This method shows the check in to the user
     *
     * @param data         Data
     * @param receptionist Receptionist
     * @throws NotAnActualResidentException Exception
     */
    public static void checkInMenu(Data data, Receptionist receptionist) throws NotAnActualResidentException {
        System.out.println("Menu Check In:");
        int rta;
        Booking book = null;
        Date today = getTodayDate();
        Passenger passenger = passengerOnSystemMenu(data);
        for (Booking booking : data.getBookingsByPassengerByStatus(passenger.getId(), false)) {
            if (booking.getStartDate().compareTo(today) == 0) {
                booking.setOccupied(true);
                book = booking;
            }
        }
        if (book != null) {
            System.out.println("El check in hecho es: " + book.toString());
        } else {
            System.out.println("No hay ninguna reserva para esa fecha, por favor ingrese los datos de su estadía: ");
            makeReservationMenuForToday(data, passenger, today);
        }
    }

    /**
     * Creates a booking for today
     *
     * @param data      Data
     * @param passenger Passenger
     * @param todayDate Date
     * @return Booking
     */
    private static Booking makeReservationMenuForToday(Data data, Passenger passenger, Date todayDate) {
        System.out.println("La fecha de llegada es: " + todayDate.toString());
        System.out.println("Ingrese la fecha de partida: ");
        Date endDate = dateMenu();
        Type type = typeMenu();
        Booking newBooking = null;
        for (Room room : data.getRoomsByType(type)) {
            if (data.roomHasBookings(room)) {
                for (Booking booking : data.getBookingsByRoom(room.getId())) {
                    if (todayDate.compareTo(booking.getEndDate()) == 1 || endDate.compareTo(booking.getStartDate()) == -1) {
                        newBooking = new Booking(passenger.getId(), room.getId(), todayDate, endDate);
                        break;
                    }
                }
            } else {
                newBooking = new Booking(passenger.getId(), room.getId(), todayDate, endDate);
                break;
            }
        }
        if (newBooking != null) {
            data.addBooking(newBooking);
            return newBooking;
        } else {
            throw new NotAvailableRoomForDateOrType();
        }
    }

    /**
     * This method shows the check out to the user
     *
     * @param data         Data
     * @param receptionist Receptionist
     * @throws NotAnActualResidentException Exception
     */
    public static void checkOutMenu(Data data, Receptionist receptionist) throws NotAnActualResidentException {
        System.out.println("Menu Check Out:");
        System.out.print("Ingrese dni del pasajero: ");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt();
        separator();
        switch (rta) {
            case 0:
                pause();
                receptionistMenu(data, receptionist);
                break;
            default:
                Passenger passenger = data.getPassengers().get(rta);
                if (passenger != null) {
                    receptionist.checkOut(data, passenger);
                } else {
                    throw new NotAnActualResidentException();
                }
                break;
        }
    }

}

