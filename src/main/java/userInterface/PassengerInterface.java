package userInterface;

import Exceptions.NotAnActualPassengerException;
import Exceptions.NotAnActualResidentException;
import data.Data;
import data.DataManager;
import reservations.Booking;
import reservations.Fare;
import reservations.Product;
import users.Passenger;

import java.util.List;

import static data.DataManager.answerInt;
import static data.DataManager.pause;
import static data.DataManager.separator;

/**
 * This class shows passenger functionality to the user.
 */
public class PassengerInterface extends UserInterface {

    /**
     * This method shows passenger menu
     *
     * @param data      Data
     * @param passenger Passenger
     * @throws NotAnActualResidentException Exception
     */
    public static void passengerMenu(Data data, Passenger passenger) throws NotAnActualResidentException {
        separator();
        System.out.println("MENU DE PASAJEROS: ");
        System.out.println("    1- Realizar reserva");
        System.out.println("    2- Servicio a la habitación");
        System.out.println("    3- Ver consumo hasta el momento");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt(3);
        separator();
        switch (rta) {
            case 1:
                Booking book = makeReservationMenu(data, passenger);
                break;
            case 2:
                roomServiceMenu(data, passenger);
                break;
            case 3:
                showAllConsumes(data, passenger.getId());
                break;
            case 0:
                Interface.startMenu(data);
                break;
        }
        passengerMenu(data, passenger);
    }

    public static Passenger passengerOnSystemMenu(Data data) {
        System.out.print("Ingrese dni del pasajero: ");
        Passenger passenger = data.getPassengers().get(answerInt());

        if (passenger == null) {
            System.out.println("No se encuentra en el sistema. Ingrese sus datos: ");
            System.out.println("Ingrese su DNI");
            Integer id = answerInt();
            System.out.println("Ingrese su Nombre");
            String name = DataManager.answerString();
            System.out.println("Ingrese su Apellido");
            String lastName = DataManager.answerString();
            System.out.println("Ingrese su Dirección");
            String address = DataManager.answerString();
            System.out.println("Ingrese su nacionalidad");
            String country = DataManager.answerString();

            passenger = new Passenger(id, name, lastName, address, country);
        }
        return passenger;
    }

    /**
     * This method shows the consume menu only if you are an actual resident
     *
     * @param data      Data
     * @param passenger Passenger
     * @throws NotAnActualResidentException Exception
     */
    public static void roomServiceMenu(Data data, Passenger passenger) throws NotAnActualResidentException {
        Booking bookingSelected = selectBookingFromPassenger(data, passenger.getId(), true);
        if (bookingSelected != null) {
            int rta;
            Product choice = null;
            System.out.println("Que desea consumir? Seleccione lo que desea de la lista: ");
            System.out.println();
            System.out.println("BEBIDAS");
            System.out.println("1- GASESOSA: Coca-Cola (1,5L) - $30");
            System.out.println("2- CERVEZA: Quilmes (1L) - $45");
            System.out.println("3- CHAMPAGNE: Norton (0,75L) - $112");
            System.out.println();
            System.out.println("COMIDAS");
            System.out.println("4- SNACKS: Papas Lays (105 gr) - $40");
            System.out.println("5- PICADA: Picada Paladinni p/2 personas - $220");
            System.out.println("6- MENU DEL DIA: Milanesa napolitana con fritas - $165");
            System.out.println();
            System.out.println("(Elija de a una opcion y para finalizar el pedido ingrese 0)");
            do {
                rta = answerInt(6);
                switch (rta) {
                    case 1:
                        choice = Product.GASEOSA;
                        break;
                    case 2:
                        choice = Product.CERVEZA;
                        break;
                    case 3:
                        choice = Product.CHAMPAGNE;
                        break;
                    case 4:
                        choice = Product.SNACKS;
                        break;
                    case 5:
                        choice = Product.PICADA;
                        break;
                    case 6:
                        choice = Product.MENU_DEL_DIA;
                        break;
                }
                if (rta != 0)
                    bookingSelected.addFare(new Fare(choice));
            } while (rta != 0);
            separator();
        } else {
            throw new NotAnActualResidentException();
        }
    }

    /**
     * This method restricts the use of the passenger menu to non-passengers
     *
     * @param data Data
     */
    public static void passengerValidation(Data data) throws NotAnActualResidentException {
        System.out.print("Bienvenido PASAJERO, ingrese su DNI: ");
        Passenger passenger;
        passenger = data.getPassengers().get(answerInt());
        if (passenger != null) {
            System.out.println("Hola " + passenger.getName() + "!");
            try {
                passengerMenu(data, passenger);
            } catch (NotAnActualResidentException e) {
                System.out.println("Volverá al menú de pasajero");
                separator();
                pause();
            }
        } else {
            new NotAnActualPassengerException().getMessage();
            passengerValidation(data);
        }
    }

    /**
     * This method shows all the fares of the passenger and the total amount
     *
     * @param data        Data
     * @param idPassenger Integer
     */
    public static void showAllConsumes(Data data, Integer idPassenger) {
        List<Booking> bookings = data.getBookingsByPassengerByStatus(idPassenger, true);
        if (bookings.size() != 0) {
            Double totalFares = 0D;
            for (Booking booking : bookings) {
                System.out.println("Habitación n° " + booking.getIdRoom());
                if (booking.getFares() != null) {
                    booking.getFares().toString();
                    totalFares += booking.getFaresAmount();
                    System.out.println("Total a pagar por esta habitación: " + booking.getFaresAmount());
                } else {
                    System.out.println("Esta habitación no posee consumos");
                }
            }
            System.out.println("-------------------");
            System.out.println("Total a pagar: " + totalFares);
            System.out.println("-------------------");
        } else {
            System.out.println("Usted no posee reservas hasta el momento");
        }
    }

}

