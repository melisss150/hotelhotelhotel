package userInterface;

import Exceptions.NotAnActualResidentException;
import data.Data;

import static data.DataManager.answerInt;
import static data.DataManager.separator;

/**
 * This class shows the interface menu to the user.
 */
public class Interface {

    /**
     * This method shows principal menu
     *
     * @param data Data
     */
    public static void startMenu(Data data) throws NotAnActualResidentException {
        System.out.println("Bienvenido al sistema de reservas del HOTEL TRESVAGOS");
        System.out.println("Que clase de usuario eres:");
        System.out.println("    1- Administrador");
        System.out.println("    2- Recepcionista");
        System.out.println("    3- Pasajero");
        System.out.println("(Oprima 0 para salir)");
        int rta = answerInt(3);
        separator();
        switch (rta) {
            case 1:
                AdministratorInterface.administratorValidation(data);
                separator();
                break;
            case 2:
                ReceptionistInterface.receptionistValidation(data);
                separator();
                break;
            case 3:
                PassengerInterface.passengerValidation(data);
                separator();
                break;
            case 0:
                separator();
                System.out.println("Espero os haya iluminado");
                System.out.println("Gracias!");
                System.out.println("Vuelva pronto!");
                break;
        }
    }

}
