package userInterface;

import Exceptions.NotAnActualResidentException;
import data.Data;
import data.DataManager;
import room.Type;
import users.Administrator;
import users.Passenger;
import users.Receptionist;

import java.io.IOException;

import static data.DataManager.*;

/**
 * This class shows administrator functionality to the user
 */
public class AdministratorInterface {

    static Administrator administrator = new Administrator();

    /**
     * Administrator menu interface.
     *
     * @param data Data
     */
    public static void administratorMenu(Data data) throws NotAnActualResidentException {
        separator();
        System.out.println("Bienvenido ADMINISTRADOR, lo estabamos esperando.");
        System.out.println("Que desea hacer?");
        System.out.println("    1- Realizar backup");
        System.out.println("    2- Crear usuarios");
        System.out.println("    3- Eliminar usuarios");
        System.out.println("    4- Crear nueva habitación");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt(4);
        separator();
        switch (rta) {
            case 1:
                backUpMenu(data);
                separator();
                break;
            case 2:
                createUserMenu(data);
                separator();
                break;
            case 3:
                deleteUser(data);
                separator();
                break;
            case 4:
                newRoomMenu(data);
                separator();
                break;
            case 0:
                separator();
                Interface.startMenu(data);
                ;
                break;
        }
        if (rta != 0) {
            pause();
            administratorMenu(data);
        }
    }

    /**
     * Method that shows available options and creates a user
     *
     * @param data Data
     */
    private static void createUserMenu(Data data) {
        System.out.println("Ingrese tipo de Usuario:");
        System.out.println("    1- Recepcionista");
        System.out.println("    2- Pasajero");
        System.out.println("(Oprima 0 para volver)");
        int userType = answerInt(2);
        if (userType != 0) {
            System.out.println("Ingrese su DNI");
            Integer id = answerInt();
            System.out.println("Ingrese su Nombre");
            String name = DataManager.answerString();
            System.out.println("Ingrese su Apellido");
            String lastName = DataManager.answerString();
            System.out.println("Ingrese su Dirección");
            String address = DataManager.answerString();
            System.out.println("Ingrese su nacionalidad");
            String country = DataManager.answerString();
            switch (userType) {
                case 1:
                    Receptionist receptionist = new Receptionist(id, name, lastName, address, country);
                    administrator.addReceptionist(data.getRecepcionists(), receptionist);
                    System.out.print("Recepcionista agregado con exito!");
                    break;
                case 2:
                    Passenger passenger = new Passenger(id, name, lastName, address, country);
                    administrator.addPassenger(data.getPassengers(), passenger);
                    System.out.println("Pasajero agregado con exito!");
                    break;
                default:
                    System.out.println("Tipo de usuario incorrecto. Vuelva a intentarlo.");
                    pause();
                    createUserMenu(data);
                    break;
            }
        }
    }

    /**
     * This method allows to choose a type of data and then creates a json archive with it till actual date and save
     *
     * @param data Data
     */
    public static void backUpMenu(Data data) {
        System.out.println("BACKUP DE: ");
        System.out.println("     1- Pasajeros");
        System.out.println("     2- Recepcionistas");
        System.out.println("     3- Habitaciones");
        System.out.println("     4- Reservaciones");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt(4);
        separator();
        switch (rta) {
            case 1:
                try {
                    administrator.backUpPassengers(data.getPassengers());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    administrator.backUpReceptionists(data.getRecepcionists());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    administrator.backUpRooms(data.getRooms());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 4:
                try {
                    administrator.backUpReservations(data.getBookings());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 0:
                break;
        }
    }

    /**
     * This method allows to delete a user from the system
     *
     * @param data Data
     */
    public static void deleteUser(Data data) {
        Integer id;
        System.out.println("Que tipo de usuario desea eliminar?");
        System.out.println("    1- Recepcionista");
        System.out.println("    2- Pasajero");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt(2);
        separator();
        switch (rta) {
            case 1:
                System.out.println("Ingrese el ID del recepcionista a eliminar: ");
                id = answerInt();
                administrator.deleteReceptionist(data.getRecepcionists(), id);
                System.out.println("Usuario removido con exito!");
                pause();
                break;
            case 2:
                System.out.println("Ingrese el ID del pasajero a eliminar: ");
                id = answerInt();
                administrator.deletePassenger(data.getPassengers(), id);
                System.out.println("Usuario removido con exito!");
                pause();
                break;
            case 0:
                break;
        }
    }

    /**
     * This method shows the interface that creates a new room
     *
     * @param data Data
     */
    public static void newRoomMenu(Data data) throws NotAnActualResidentException {
        System.out.println("Ingrese ID de habitación: ");
        int id = answerInt();
        System.out.println("Ingrese tipo de habitación: ");
        System.out.println("    1- Simple.");
        System.out.println("    2- Doble");
        System.out.println("    3- Suite");
        System.out.println("(Oprima 0 para volver)");
        int rta = answerInt(3);
        Type type = null;
        switch (rta) {
            case 1:
                type = Type.SIMPLE;
                break;
            case 2:
                type = Type.DOUBLE;
                break;
            case 3:
                type = Type.SUITE;
                break;
            case 0:
                administratorMenu(data);
                break;
        }
        if (rta != 0) {
            data.createNewRoomAndAdd(id, type);
        }
    }

    /**
     * This method validates administrator credentials
     *
     * @param data Data
     */
    public static void administratorValidation(Data data) throws NotAnActualResidentException {
        System.out.print("Ingrese la clave para ingresar(Clave: 1234): ");
        int rta = answerInt();
        if (rta == 1234) {
            administratorMenu(data);
        } else {
            System.out.println("Usted no tiene permiso de acceder al sistema.");
            System.out.println("Volverá al menu principal.");
            separator();
            Interface.startMenu(data);
        }
    }

}