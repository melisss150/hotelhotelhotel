package reservations;

public enum Product {

    GASEOSA(30),

    CERVEZA(45),

    CHAMPAGNE(112),

    SNACKS(40),

    PICADA(220),

    MENU_DEL_DIA(165);

    private double price;

    Product() {
    }

    Product(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

}
