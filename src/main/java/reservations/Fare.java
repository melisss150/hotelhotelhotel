package reservations;

import java.util.Date;


/**
 * Class that manage the fares of the passengers
 */
public class Fare {

    private Product product;
    private Date date;

    /**
     * Constructor for fare object
     *
     * @param product Product
     */
    public Fare(Product product) {
        this.product = product;
        this.date = new Date();
    }

    /**
     * Returns the product
     *
     * @return Product
     */
    public Product getProduct() {
        return product;
    }

    @Override
    public String toString() {
        return "Fecha: " + date + ", Producto: " + product + ", Precio: " + product.getPrice();
    }

}
