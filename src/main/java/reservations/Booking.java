package reservations;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.sql.Date;
import java.util.List;

/**
 * Class that manages passengers bookings
 */
public class Booking {

    private Integer idPassenger;
    private Integer idRoom;
    private Date startDate;
    private Date endDate;
    private boolean isOccupied;
    private boolean isCharged;
    private List<Fare> fares;

    /**
     * Empty constructor for booking object
     */
    public Booking() {
    }

    /**
     * Constructor for booking object
     *
     * @param idPassenger Integer
     * @param idRoom      Integer
     * @param startDate   Date
     * @param endDate     Date
     */
    public Booking(Integer idPassenger, Integer idRoom, Date startDate, Date endDate) {
        this.idPassenger = idPassenger;
        this.idRoom = idRoom;
        this.isCharged = false;
        this.isOccupied = false;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void setIdPassenger(Integer idPassenger) {
        this.idPassenger = idPassenger;
    }

    public void setIdRoom(Integer idRoom) {
        this.idRoom = idRoom;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setFares(List<Fare> fares) {
        this.fares = fares;
    }

    /**
     * Gets passenger's id
     *
     * @return Integer
     */
    public Integer getIdPassenger() {
        return idPassenger;
    }

    /**
     * Gets id's room
     *
     * @return Integer
     */
    public Integer getIdRoom() {
        return idRoom;
    }

    /**
     * Gets booking start day
     *
     * @return Date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Gets booking ends day
     *
     * @return Date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets if a booking is already occupied
     *
     * @param occupied boolean
     */
    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }

    /**
     * Sets if the booking is charged
     *
     * @param charged boolean
     */
    public void setCharged(boolean charged) {
        isCharged = charged;
    }

    /**
     * Returns if a room is charged
     *
     * @return boolean
     */
    @JsonIgnore
    public boolean isCharged() {
        return isCharged;
    }

    /**
     * Returns if a booking is actually occupied
     *
     * @return boolean
     */
    @JsonIgnore
    public boolean isOccupied() {
        return isOccupied;
    }

    /**
     * Gets a fares list
     *
     * @return Listof Fares
     */
    public List<Fare> getFares() {
        return fares;
    }

    /**
     * This method return the fare total amount.
     *
     * @return double
     */
    @JsonIgnore
    public double getFaresAmount() {
        double rta = 0;
        for (Fare fare : fares) {
            rta += fare.getProduct().getPrice();
        }
        return rta;
    }

    /**
     * This method adds a fare to the list of fares of de booking
     *
     * @param fare Fare
     */
    public void addFare(Fare fare) {
        fares.add(fare);
    }

    @Override
    public String toString() {
        return "Booking{" +
                "DNI=" + idPassenger +
                ", Habitación=" + idRoom +
                ", Fecha de Ingreso=" + startDate +
                ", Fecha de Egreso=" + endDate;
    }

}
