package users;

import data.Data;

/**
 * This class is Passenger and extends User.
 */
public class Passenger extends User {

    /**
     * This method is the default constructor of Passenger.
     */
    public Passenger() {
    }

    /**
     * This method is the constructor of Passenger.
     *
     * @param id       Integer
     * @param name     String
     * @param lastName String
     * @param address  String
     * @param country  String
     */
    public Passenger(Integer id, String name, String lastName, String address, String country) {
        super(id, name, lastName, address, country);
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
