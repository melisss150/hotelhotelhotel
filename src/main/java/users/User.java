package users;

/**
 * This is the father class of passenger, administrator and receptionist.
 */

public class User {

    private Integer id;
    private String name;
    private String lastName;
    private String address;
    private String country;

    /**
     * This is the constructor of User.
     */

    public User() {
    }

    /**
     * This is the other constructor of User.
     *
     * @param id       Integer
     * @param name     String
     * @param lastName String
     * @param address  String
     * @param country  String
     */
    public User(Integer id, String name, String lastName, String address, String country) {
        this.name = name;
        this.lastName = lastName;
        this.id = id;
        this.address = address;
        this.country = country;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;

        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name) && !address.equals(other.address) && !lastName.equals(lastName) && !country.equals(country))
            return false;
        return true;

    }

    /**
     * This method set the id of the user.
     *
     * @param id Integer
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method set the name of the user.
     *
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method return the name of the user.
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * This method return the lastname of the user.
     *
     * @return String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * This method return the id of the user.
     *
     * @return Integer
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method return the address of the user.
     *
     * @return String
     */
    public String getAddress() {
        return address;
    }

    /**
     * This method return the country of the user.
     *
     * @return String
     */
    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() + ", Nombre: " + this.getName() + " " + this.getLastName() + ", Dirección: " +
                this.getAddress() + ", País: " + this.getCountry();
    }

}
