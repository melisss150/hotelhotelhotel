package users;


import data.FileManager;
import reservations.Booking;
import room.Room;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * This class is Passenger and extends User
 */
public class Administrator extends User {

    /**
     * Constructor for Administrator object
     *
     * @param id       Integer
     * @param name     String
     * @param lastName String
     * @param address  String
     * @param country  String
     */
    public Administrator(Integer id, String name, String lastName, String address, String country) {
        super(id, name, lastName, address, country);
    }

    /**
     * Default constructor for Administrator object
     */
    public Administrator() {

    }

    /**
     * This method creates a Back up File of Passengers
     *
     * @param passengers Map of Passengers
     * @throws IOException Exception
     */
    public static void backUpPassengers(Map<Integer, Passenger> passengers) throws IOException {
        String path = "src/main/resources/backUps/PassengersBackUp(" + LocalDate.now().toString() + ")";
        FileManager.savePassengers(passengers, path);
    }

    /**
     * This method creates a Back up File of Receptionists
     *
     * @param receptionists Map of Recepcionists
     * @throws IOException Exception
     */
    public void backUpReceptionists(Map<Integer, Receptionist> receptionists) throws IOException {
        String path = "src/main/resources/backUps/ReceptionistsBackUp(" + LocalDate.now().toString() + ")";
        FileManager.saveReceptionists(receptionists, path);
    }

    /**
     * This method creates a Back up File of all Rooms
     *
     * @param rooms Map of Rooms
     * @throws IOException Exception
     */
    public void backUpRooms(Map<Integer, Room> rooms) throws IOException {
        String path = "src/main/resources/backUps/RoomsBackUp(" + LocalDate.now().toString() + ")";
        FileManager.saveRooms(rooms, path);
    }

    /**
     * This method creates a back up files of the Reservations
     *
     * @param reservations List of Bookings
     * @throws IOException Exception
     */
    public void backUpReservations(List<Booking> reservations) throws IOException {
        String path = "src/main/resources/backUps/ReservationsBackUp(" + LocalDate.now().toString() + ")";
        FileManager.saveBookings(reservations, path);
    }

    /**
     * This method deletes a receptionist by his id
     *
     * @param recepcionists Map of Recepcionists
     * @param id            Integer
     */
    public void deleteReceptionist(Map<Integer, Receptionist> recepcionists, Integer id) {
        recepcionists.remove(id);
    }

    /**
     * This method deletes a passenger by his id
     *
     * @param passengers Map of Passengers
     * @param id         Integer
     */
    public void deletePassenger(Map<Integer, Passenger> passengers, Integer id) {
        passengers.remove(id);
    }

    /**
     * This method add a new receptionist to the system
     *
     * @param recepcionists Map of Recepcionist
     * @param recepcionist  Receptionist
     */
    public void addReceptionist(Map<Integer, Receptionist> recepcionists, Receptionist recepcionist) {
        recepcionists.put(recepcionist.getId(), recepcionist);
    }

    /**
     * This method add a new passenger to the system
     *
     * @param passengers Map of Passengers
     * @param passenger  Passenger
     */
    public void addPassenger(Map<Integer, Passenger> passengers, Passenger passenger) {
        passengers.put(passenger.getId(), passenger);
    }

}
