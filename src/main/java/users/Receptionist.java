package users;


import Exceptions.NotAValidRoomException;
import Exceptions.NotAnActualResidentException;
import data.Data;
import data.DataManager;
import reservations.Booking;
import room.Room;
import room.Status;

import java.sql.Date;
import java.util.Calendar;

import static data.DataManager.getTodayDate;
import static userInterface.UserInterface.selectBookingFromPassenger;

/**
 * This class is Receptionist and extends User.
 */
public class Receptionist extends User {

    /**
     * Default constractor for receptionist object
     */
    public Receptionist() {
    }

    /**
     * This method is the constructor of Receptionist object
     *
     * @param id       Integer
     * @param name     String
     * @param lastName String
     * @param address  String
     * @param country  String
     */
    public Receptionist(Integer id, String name, String lastName, String address, String country) {
        super(id, name, lastName, address, country);
    }

    /**
     * This method makes the check out of one passenger.
     *
     * @param data      Data
     * @param passenger Passenger
     * @throws NotAnActualResidentException Exception
     */
    public void checkOut(Data data, Passenger passenger) throws NotAnActualResidentException {
        Date today = getTodayDate();

        Booking book = selectBookingFromPassenger(data, passenger.getId(), true);
        if (book != null) {
            Room room = data.getRooms().get(book.getIdRoom());
            room.setStatus(Status.AVAILABLE);
            double total = book.getFaresAmount();
            if (book.getEndDate().compareTo(today) != 0) {
                System.out.println("La fecha de salida es distinta a la fecha reservada originalmente, se le cobrara un adicional del 20%");
            }
            System.out.println("Monto total de consumo: $" + total);
            book.setCharged(true);
        } else {
            throw new NotAnActualResidentException();
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
